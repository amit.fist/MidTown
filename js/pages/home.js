//Home banner
var homeBanner = new MasterSlider();
homeBanner.setup('home-banner', {
    width: 1366,
    height: 628,
    //space:100,
    fullwidth: true,
    centerControls: false,
    speed: 18,
    autoplay: true,
    view: 'flow'
});
//slider.control('arrows');
homeBanner.control('bullets', { autohide: true });
// add scroll parallax effect
MSScrollParallax.setup(homeBanner, 50, 80, true);

//Now showing
var nowShowingslider = new MasterSlider();
nowShowingslider.setup('now-showing-lists', {
    width: 995,
    height: 630,
    autoHeight: true,
    space: 0,
    preload: 'all',
    autoplay: false,
    view: 'basic'
});
nowShowingslider.control('arrows', { insertTo: "#now-showing-head", autohide: false });
nowShowingslider.control('thumblist', { insertTo: "#now-showing-head", autohide: false, dir: 'h', type: 'tabs', width: 70, height: 74, space: 0, hideUnder: 400 });

//Aside offer/Advertisement Slider
var offerAdvert = new MasterSlider();
offerAdvert.setup('offer-advert', {
    width: 995,
    autoHeight: true,
    speed: 18,
    autoplay: false,
    view: 'wave'
});

//Mall Slide
var mallSlide = new MasterSlider();
mallSlide.setup('mall-slide', {
    width: 1300, // slider standard width
    height: 474, // slider standard height
    space: 5,
    autoHeight: true,
    view: "basic"
});
// adds Arrows navigation control to the slider.
mallSlide.control('arrows');

//Coming soon 
var commingSoon = new MasterSlider();
commingSoon.setup('commingsoon-slider', {
    width: 1024,
    height: 703,
    //space:100,
    fullwidth: true,
    centerControls: false,
    speed: 18,
    instantStartLayers: true,
    autoplay: false,
    view: 'scale'
});
commingSoon.control('arrows');
// add scroll parallax effect
MSScrollParallax.setup(commingSoon, 0, 80, false);
commingSoon.control('thumblist', { autohide: false, dir: 'v', type: 'tabs', width: 30, height: 20, align: 'top', space: 0, hideUnder: 400, arrows: false });