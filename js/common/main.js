var windowWidth = $(window).width();

if (windowWidth > 1023) {
    $(window).scroll(function () {
        var scroll = $(window).scrollTop();
        if (scroll >= 40) {
            $('.main-header').addClass('sticky-header');
            $(".logo-holder").removeClass("full");
        } else {
            $('.main-header').removeClass('sticky-header');
            $(".logo-holder").addClass("full");
        }
    });
}

//Mobile Navigation
$(".mobile-nav-toggler").click(function () {
    $(this).parent().parent().toggleClass('open');
    $(this).toggleClass('active');
});

//Close header on click links
$(".main-nav ul li a").click(function () {
    if (windowWidth < 1024) {
        $('.main-header').removeClass('open');
        $(".mobile-nav-toggler").removeClass('active');
    }
});

//Home Section Scroll
$(".scroll").click(function (e) {
    e.preventDefault();
    var sectionTarget = $(this).attr('href');
    $(".scroll").removeClass('active');
    $(this).addClass('active');   
    $('html, body').animate({
        scrollTop: $(sectionTarget).offset().top - 170
    }, 2000);
});

//Floating Login Form 
$('#login-form').fadeOut();
$(".login-toggler").click(function () {
    if ($('#login-form').hasClass('bounceOut')) {
        $('#login-form').fadeIn(1);
        $('#login-form').removeClass('bounceOut');
        $('#login-form').addClass('pulse');
        $(this).addClass('active');
        $('.popup-overlay').fadeIn(1);
    } else {
        $('#login-form').addClass('bounceOut');
        $('#login-form').removeClass('pulse');
        $('#login-form').delay(500).fadeOut();
        $(this).removeClass('active');
        $('.popup-overlay').fadeOut(1);
    }
});

$('.popup-overlay').click(function () {
    $('#login-form').addClass('bounceOut');
        $('#login-form').removeClass('pulse');
        $('#login-form').delay(500).fadeOut();
        $(this).removeClass('active');
        $('.popup-overlay').fadeOut(1);
});

//Close Login Form for mobile screen
$(".btn-close").click(function () {
    $('.login-toggler').removeClass('active');
    $(this).parent().removeClass('pulse');
    $(this).parent().addClass('bounceOut');
    $('.popup-overlay').fadeOut(1);
});

//Loader
$(window).load(function () {
    $(".loader").addClass("close-load");
    $(".logo-holder").addClass("full");
    $(".loader").delay(500).fadeOut(500);
    $("body").removeClass("over-hidden");
    ;
});

//Select 2
$("select").select2({
    minimumResultsForSearch: Infinity
});